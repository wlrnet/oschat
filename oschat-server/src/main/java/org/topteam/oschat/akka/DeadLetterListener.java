package org.topteam.oschat.akka;

import com.alibaba.fastjson.JSON;

import akka.actor.DeadLetter;
import akka.actor.UntypedActor;

public class DeadLetterListener extends UntypedActor{

	@Override
	public void onReceive(Object message) throws Exception {
		if(message instanceof DeadLetter){
			DeadLetter deadLetter = (DeadLetter)message;
			Object msg = deadLetter.message();
			System.out.println(JSON.toJSONString(msg));
			//TODO 缓存未到达信息
			
		}
	}

}
