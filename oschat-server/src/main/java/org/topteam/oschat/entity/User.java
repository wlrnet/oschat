package org.topteam.oschat.entity;

import java.io.Serializable;

public class User implements Serializable{
	
	private static final long serialVersionUID = -4138897124554139234L;

	public final static String TAG = "user";

	private String username;
	private String password;
	
	private String userId;
	
	private String portrait;
	
	private String resume;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}
	
}
