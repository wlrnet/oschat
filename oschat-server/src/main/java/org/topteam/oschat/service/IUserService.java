package org.topteam.oschat.service;

import java.util.List;
import java.util.Map;

import org.topteam.oschat.entity.User;
import org.topteam.oschat.entity.UserRelation;

public interface IUserService {

	public User getUser(String userId);

	public UserRelation getUserRelation(String userId);
	
	public Map<String, List<User>> getFriends(String userId);

	public void updateStat(String userId,UserStatue onLine);

	public List<String> getOnlineFirends(String userId);	
}
