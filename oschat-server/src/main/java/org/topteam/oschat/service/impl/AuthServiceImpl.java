package org.topteam.oschat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.topteam.oschat.dao.KvDao;
import org.topteam.oschat.service.IAuthService;

@Service
public class AuthServiceImpl implements IAuthService {
	
	@Autowired private KvDao dao;

	@Override
	public void addToken(String token, String userId) {
		dao.open("auth").put(token, userId);
	}

	@Override
	public String getToken(String token) {
		return dao.open("auth").get(token);
	}

}
